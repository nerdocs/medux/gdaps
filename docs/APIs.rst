API
===

Interfaces/Implementations
--------------------------

.. autofunction:: gdaps.api.Interface


Plugin configuration and metadata
---------------------------------

.. autoclass:: gdaps.api.PluginMeta
    :members:

.. autoclass:: gdaps.api.PluginConfig
    :members:

PluginManager
-------------

.. automodule:: gdaps.pluginmanager
    :members:

Templates
---------

.. autoclass:: gdaps.api.interfaces.ITemplatePluginMixin
    :members:
    :undoc-members:

Helper functions
----------------

.. autofunction:: gdaps.api.require_app
