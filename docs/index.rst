GDAPS - Generic Django Apps Plugin System
=========================================

Welcome to the GDAPS documentation!

GDAPS is a plugin system that can be added to Django, **to create applications that can be extended via plugins later**.

.. note::
    As of v0.7.0, Vue.js support was dropped, in favour of the :ref:`new template rendering plugin system <template-support>`.



.. toctree::
    :maxdepth: 2
    :caption: Table of Contents

    introduction
    installation
    usage
    drf
    APIs
    contributing


License
=======

I'd like to give back what I received from many Open Source software packages, and keep this
library as open as possible, and it should stay this way.
GDAPS is licensed under the `BSD License (BSD) <https://opensource.org/licenses/BSD-3-Clause>`_.
