test:
	pytest

coverage:
	coverage run -m pytest

livedocs:
	cd docs && make livehtml

build:
	flit build

publish:
	flit publish
